//
//  ViewController.h
//  UsingYATextField
//
//  Created by Mac-6 on 09/01/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomRegistrationForm.h"

@interface ViewController  : UIViewController

@property (nonatomic,strong) IBOutlet CustomRegistrationForm *form ;
@end

