//
//  CustomRegistrationForm.h
//  UsingYATextField
//
//  Created by Mac-6 on 09/01/16.
//  Copyright © 2016 Big Nerd Ranch. All rights reserved.
//

#import "YALBaseForm.h"


@class YALField ;


@interface CustomRegistrationForm : YALBaseForm

@property (nonatomic,weak) IBOutlet YALField *name ;
@property (nonatomic,weak) IBOutlet YALField *phone ;
@property (nonatomic,weak) IBOutlet YALField *email ;
@property (nonatomic,weak) IBOutlet YALField *password ;


@end
